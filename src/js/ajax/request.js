class Request {
  constructor(baseURL, token) { 
    this.baseURL = baseURL;
    this.token = token; 
    this.request = axios.create({
      baseURL,
      headers : {Authorization: `Bearer ${token}`}
    });
    this.request.defaults.baseURL = baseURL;
    
  }
  updateAxios() {
    this.request.defaults.headers['Authorization'] = `Bearer ${this.token}`
    }
  
  
  get(mod = "", options = {}) {
    return this.request.get(mod, options);
  }
  put(mod = "", options = {}) {
    return this.request.put(mod, options);
  }
  post(mod = "", options = {}) {
    return this.request.post(mod, options);
  }
  delete(mod = "", options = {}) {
    return this.request.delete(mod, options);
  }
}
export default Request;
